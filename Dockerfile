ARG ALPINE_VER=3
FROM "alpine:${ALPINE_VER}" as main_image

## Copy our scripts to userland.
COPY ./scripts/ /usr/bin/publisher-scripts/
RUN chmod 555 -R /usr/bin/publisher-scripts/
ENV PATH="${PATH}:/usr/bin/publisher-scripts/"

## Now run the chore of apt update, install and blablaba.
RUN apk update

RUN apk add --no-cache \
    git \
    git-lfs \
    openssh-client \
    gnupg \
    curl \
    bash \
    rsync

## Install autotag from autotag's one-liner installer
## See {@link https://github.com/pantheon-systems/autotag#one-liner}
## This, by no any means, not the best way to install the binary, since we
## can't actually sure what's the sh script is doing.
## I'm just too lazy to properly fetch the binary from GH.
RUN curl -sL https://git.io/autotag-install | sh -s -- -b /usr/bin

RUN mkdir -p ~/.ssh

## Setup the Git LFS
## @see https://git-lfs.com/
RUN git lfs install

## Copy the hardened known_host thing for ssh. This will be helpful for
## publishing tags to git immediately.
COPY ./hardened/ssh/known_hosts known_hosts
RUN mv known_hosts ~/.ssh/known_hosts
RUN chmod 644 ~/.ssh/known_hosts

## Start the SSH Agent
ENTRYPOINT [ "_entrypoint.sh" ]
