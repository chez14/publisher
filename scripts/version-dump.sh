#!/bin/sh

## Exit immediately when error happens.
set -e;

echo "Git version:" 
git --version;
echo -e "\n\n";

echo "SSH version";
ssh -V;
echo -e "\n\n";

echo "GPG version";
gpg --version
echo -e "\n\n";
