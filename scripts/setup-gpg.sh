#!/bin/sh

## Exit immediately when error happens.
set -e;

chmod 600 "${PUBLISH_GPG_SECRET_FILE}";
gpg --no-tty --import "${PUBLISH_GPG_SECRET_FILE}";

echo "GPG Key import done.";