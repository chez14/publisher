#!/bin/sh

## Exit immediately when error happens.
set -e

## set the publish key as
chmod 600 ${PUBLISH_KEY}
ssh-add ${PUBLISH_KEY}

echo "SSH Key import done."
