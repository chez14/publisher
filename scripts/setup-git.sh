#!/bin/sh

## Exit immediately when error happens.
set -e

if [[ -n "${PUBLISH_USERNAME}" ]]; then
    git remote set-url origin "https://${PUBLISH_USERNAME}:${PUBLISH_KEY}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
    echo "Git remote url now set to ${PUBLISH_USERNAME}:[REDACTED]@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
else
    git remote set-url origin "git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git"
    echo "Git remote url now set to $(git remote get-url origin)"
fi

## Set the git commit and signing info
git config --global user.email "${PUBLISH_AUTHOR_EMAIL:-$GITLAB_USER_EMAIL}"
git config --global user.name "${PUBLISH_AUTHOR_NAME:-$GITLAB_USER_NAME}"
echo "Git author now set to \"$(git config user.name) <$(git config user.email)>\""

if [[ -n "${PUBLISH_GPG_KEYID}" ]]; then
    ## Since one keyfile may have several keys, we'll just ask for key id
    ## instead.
    git config user.signingkey "${PUBLISH_GPG_KEYID}"
    ## Set git to automatically sign the tag & commit it made.
    ## {@link https://git-scm.com/docs/git-config#Documentation/git-config.txt-taggpgSign}
    ## {@link https://git-scm.com/docs/git-tag#Documentation/git-tag.txt--s}
    git config tag.gpgSign true
    ## {@link https://git-scm.com/docs/git-config#Documentation/git-config.txt-commitgpgSign}
    git config commit.gpgSign true

    echo "Git now signs everything with GPG keyid $(git config user.signingkey)"
fi

echo "Git Setup done."
