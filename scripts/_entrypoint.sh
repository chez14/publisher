#!/bin/sh

## Exit immediately when error happens.
set -e

eval $(ssh-agent -s)

exec "$@"
