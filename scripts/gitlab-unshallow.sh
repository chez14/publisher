#!/bin/sh

## This script will help unshallow the git information for `autotag` purposes.
## `autotag` requires extra information to determine current version number.

## Warning: This only work on GitLab.

## Exit immediately when error happens.
set -e;

git checkout $CI_COMMIT_BRANCH;
git reset --hard $CI_COMMIT_SHA;
echo "Current git branch is on $(git rev-parse --abbrev-ref HEAD)";
echo "Current git commit is on $(git rev-parse HEAD)";

git fetch --tags --prune;

# Ensure a local default branch exists (e.g., master or main).
if [ "$(git rev-parse --abbrev-ref HEAD)" != "$CI_DEFAULT_BRANCH" ]; then
  # Check if the default branch already exists locally.
  if git show-ref --quiet "refs/heads/$CI_DEFAULT_BRANCH"; then
    # Set up tracking for the existing default branch.
    git branch --set-upstream-to="origin/$CI_DEFAULT_BRANCH" "$CI_DEFAULT_BRANCH";
  else
    # Create a local branch tracking the default branch.
    git branch --track "$CI_DEFAULT_BRANCH" "origin/$CI_DEFAULT_BRANCH";
  fi
fi

echo "Unshallowing git info for GitLab done.";
