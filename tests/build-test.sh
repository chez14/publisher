#!/bin/sh

set -e;

SCRIPT=$(readlink -f "$0");
SCRIPTPATH=$(dirname "$SCRIPT");

docker build --pull -t "chez14/publisher:test" "${SCRIPTPATH}/.."
