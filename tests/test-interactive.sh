#!/bin/sh

set -e;

image_name=${image_name:-'chez14/publisher:test'}
container_name=${container_name:-'publisher-testing'}

docker run \
    --rm \
    -it \
    --name ${container_name} \
    -v "$(pwd):/build/chez14/publisher:ro" \
    -w "/build/chez14/publisher" \
    -e "CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH}" \
    -e "CI_COMMIT_SHA=${CI_COMMIT_SHA}" \
    -e "CI_SERVER_HOST=${CI_SERVER_HOST}" \
    -e "CI_PROJECT_PATH=${CI_PROJECT_PATH}" \
    -e "PUBLISH_USERNAME=${GITLAB_USER_LOGIN}" \
    -e "PUBLISH_KEY=${CI_JOB_TOKEN}" \
    "${image_name}"
