# Publisher

Docker image for GitLab CI's publishing duty.

Equipped with Autotag, Git, PGP, SSH, and GitLab and GitHub's Authorized Keys

## Getting Started

Create a new job for GitLab CI/CD publihsing. Use this repo's image for the
image and runs your publihsing duty there.

For example, this are taken from my [PHP Code Style repo's GitLab CI/CD
Configuration](https://gitlab.com/chez14/phpcs/-/blob/master/.gitlab-ci.yml).

```yaml
publish_tag:
  stage: deploy
  image: registry.gitlab.com/chez14/publisher:v1.0.2
  # .... (redacted for clarity)
  script:
    - setup-ssh.sh
    - setup-git.sh
    - gitlab-unshallow.sh
    - autotag
    - git push --tags
```

Then you will need to add several CI/CD Variables for this to run:
- `PUBLISH_USERNAME`: GitLab Username for authentication via HTTPS.\
  Required if you want to use HTTPS authentciation, you will **need** to skip
  this if you want to use SSH authentication.

- `PUBLISH_KEY`: GitLab Password/Token/SSH Key for Authentication.\
  Requred. If you use HTTPS authentication, then you will need to fill this with
  either your password or personal token. If you just need the read-only
  features, you can use [CI Job
  Token](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html) instead. \
  For SSH Authentication, you will need to fill this with SSH Private Key, and
  add the Public key to either [Deploy
  Key](https://docs.gitlab.com/ee/user/project/deploy_keys/) section of you
  repository, or add them to SSH Key section of your bot account (or your
  account).

Validate your GitLab CI/CD Config, then push the commit to your repo and see its
working.

For full list of variables, you can see them on the [Variables](#variables)
section of this README.

Important Notes:
- Anything that needs to use SSH as authentication will need to be in the same
  script section as `setup-ssh.sh` script. \
  I haven't found any way to start the `ssh-agent` to run on the background for
  each script runs, hence the ssh key will need to be readded each time you want
  to use the ssh authentication.

- You will need to setup your authentication first (if there's any), then git,
  before you can actually unshallow your git commit history.


## Variables

| Variable Name                                                | Required                               | Description                                                  |
| ------------------------------------------------------------ | -------------------------------------- | ------------------------------------------------------------ |
| `PUBLISH_USERNAME`<br />Type: String 🧵<br />Default: *null*  | ✅<br />Unless you’re using SSH Auth. | HTTPS Authentication username for git. You will need to **skip this when authenticating with SSH key**. |
| `PUBLISH_KEY` 🔐<br />Type: String 🧵or File 📝<br />Default: *null* | ✅ | If you’re using HTTPS Auth, fill with password, a personal token, or CI/CD Job Token.<br />If you’re using SSH Auth, you will need to fill it with SSH Private key, and put the public key on the Deploy Key or Account’s SSH Auth. |
| `CI_SERVER_HOST`<br />Type: String 🧵<br />Default: `gitlab.com` 🦝 | ❌| Used to connect with git’s original host, this particular one is defining the origin git host. |
| `CI_PROJECT_PATH`<br />Type: String 🧵<br />Default: *your project path* 🦝 | ❌ | Used to connect with git’s original host, this particular one is defining the full project path without the host section. |
| `PUBLISH_AUTHOR_EMAIL`<br />Type: String 🧵<br />Default: `GITLAB_USER_EMAIL` 🦝 | ❌ | Used to mark the Git’s Author of particular commit or tag that are going to be made. |
| `PUBLISH_AUTHOR_NAME`<br />Type: String 🧵<br />Default: `GITLAB_USER_NAME` 🦝 | ❌ | Used to mark the Git’s Author of particular commit or tag that are going to be made. |
| `PUBLISH_GPG_SECRET_FILE` 🔐<br />Type: File 📝<br />Default: *null* | ❌ | Provide GPG Private Key File to setup the GPG key support.   |
| `PUBLISH_GPG_KEYID` <br />Type: String 🧵<br />Default: *null* | ❌ | GPG Key ID to be registered with Git’s Tag.                  |
| `CI_COMMIT_BRANCH`<br />Type: String 🧵<br />Default: *Current commit branch* 🦝 | ❌ | Used to unshallow the git commit history.                    |
| `CI_COMMIT_SHA`<br />Type: String 🧵<br />Default: *Current commit SHA ID.* 🦝 | ❌ | Used to unshallow the git commit history.                    |
| `CI_DEFAULT_BRANCH`<br />Type: String 🧵<br />Default: *Default branch of the reporsitory.* 🦝 | ❌ | Used to add track to default branch of the repo.             |

Notes on icon labels:

- 🦝 \
  Meaning the variable are prefilled with GitLab’s predefined value, unless you
  override the value.
- 🔐 \
  Variable should be protected or secured.

# License

While this image are licensed as MIT. There are several softwares that may have
diffreent licenses. You may need to check that one.

- OpenSSH (BSD-style license)\
  https://www.openssh.com/

- GNUPG (GPL-3)\
  https://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob_plain;f=COPYING;hb=HEAD
  https://github.com/gpg/gnupg/blob/master/COPYING

- Git (GPL-2)\
  https://git.kernel.org/pub/scm/git/git.git/tree/COPYING

- Autotag by Pantheon Systems (Apache 2)\
  https://github.com/pantheon-systems/autotag/blob/master/LICENSE

- Git-LFS (MIT)\
  https://github.com/git-lfs/git-lfs/blob/main/LICENSE.md

- rsync (GPL-3)\
  https://github.com/WayneD/rsync/blob/master/COPYING

And finally, this project, the files contained in this repo in particular are
licensed as [MIT](./LICENSE).
